#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@RealizarBusca
Feature: Realizar Busca
  Sendo um usuário do Shoestock
  Posso realizar a busca de um produto no site
  Para que eu um resultado a validar

  @RealizarBusca
  Scenario Outline: Realizar Busca
    Given que estou na home page do Shoestock
    When eu digito a <busca>
    When eu clico em pesquisar
    Then eu Valido o resultado da pesquisa

    Examples: 
      | busca |
      | "Iphone"  |
      | "Sapato"  |
      