#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@validar
Feature: Validar Produto
  Sendo um usuário do Shoestock
  Posso validar as informações
  Para que eu não compre o produto errado

  @validar
  Scenario Outline: Validar Produto
    Given que estou na home page do Shoestock
    When eu digito a <busca>
    When eu clico em pesquisar
    Then eu Valido o resultado da pesquisa
    When eu clico no produto do resultado
    When eu incluo o produto no carrinho
    When valido os dados do produto

    Examples: 
      | busca |
      | "CINTO COURO SHOESTOCK METALIZADO FEMININO - PRATA"  |
      | "Sapato"  |
      