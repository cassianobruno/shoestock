package br.com.web.shoestock.feature;

import br.com.web.shoestock.page.CarrinhoPage;
import br.com.web.shoestock.page.PaginaBase;
import br.com.web.shoestock.page.PaginaParametros;
import br.com.web.shoestock.Product;

import junit.framework.Assert;

public class ValidarProdutoFeature extends PaginaBase{
	
	PaginaParametros paginaParametros;
	CarrinhoPage carrinhoPage;
	Product product;

	public void ValidarProdutoIncluido() {
		carrinhoPage = new CarrinhoPage();
		System.out.println(carrinhoPage.ValidarProdutoIncluido());
		System.out.println(product.produString);
		Assert.assertTrue(product.produString.toLowerCase().contains(carrinhoPage.ValidarProdutoIncluido().toLowerCase()));
	}

}
