package br.com.web.shoestock.feature;

import static org.junit.Assert.fail;

import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import br.com.web.shoestock.page.HomePagina;
import br.com.web.shoestock.page.PaginaBase;
import br.com.web.shoestock.page.PaginaParametros;
import junit.framework.Assert;

public class RealizarBuscaFeature extends PaginaBase{
	
	PaginaParametros paginaParametros;
	HomePagina paginaHome;

	public void digitarBusca(String pesquisa) {
		paginaHome = new HomePagina();
		paginaHome.campoBusca(pesquisa);
	}

	public void clicoEmPesquisar() {
		paginaHome = new HomePagina();
		paginaHome.clicoEmPesquisar();
	}

	public void validarResultado() {
		paginaHome = new HomePagina();
		Assert.assertTrue(paginaHome.validarResultado().toLowerCase().contains("Resultados".toLowerCase()));
	}

}
