package br.com.web.shoestock.feature;

import br.com.web.shoestock.page.PaginaBase;
import br.com.web.shoestock.page.PaginaParametros;
import br.com.web.shoestock.page.BuscaPage;
import br.com.web.shoestock.page.HomePagina;
import br.com.web.shoestock.Product;

public class IncluirProdutoCarrinhoFeature extends PaginaBase{
	
	PaginaParametros paginaParametros;
	BuscaPage buscaPage;
	Product product;

	public void clicoProduto() {
		buscaPage = new BuscaPage();
		buscaPage.clicoProduto();
	}


	public void tituloProduct() {
		buscaPage = new BuscaPage();
		paginaParametros = new PaginaParametros();
		paginaParametros.aguardarProcessamento();
		product.produString = buscaPage.tituloProduct();
		paginaParametros.aguardarProcessamento();
	}
	
	public void adicionarCarrinho() {
		buscaPage = new BuscaPage();
		paginaParametros = new PaginaParametros();
		paginaParametros.aguardarProcessamento();
		buscaPage.escolhaCor();
		paginaParametros.aguardarProcessamento();
		buscaPage.tamanho();
		paginaParametros.aguardarProcessamento();
		buscaPage.adicionarCarrinho();
		paginaParametros.aguardarProcessamento();
	}

}
