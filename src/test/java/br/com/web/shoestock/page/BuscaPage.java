package br.com.web.shoestock.page;

public class BuscaPage {
	
	protected PaginaBase paginaBase;

	private String resultProduct = "//*[@id=\"item-list\"]/div[1]/div[1]/div[2]/a[1]";
	private String tituloProduct = "//*[@id=\"content\"]/div[2]/section/section[1]/h1";
	private String buttonComprar = "//*[@id=\"buy-button-now\"]";
	private String buttonColorCSS = "#buy-box > section.product-sku-selector > div > ul > li:nth-child(1) > a";
	private String buttonColor = "//*[@id=\"buy-box\"]/section[1]/div/ul/li[1]/a";
	private String buttonTamanhoCSS = "#buy-box > section.product-size-selector > div > ul > li:nth-child(1) > a";
	private String buttonTamanho = "//*[@id=\"buy-box\"]/section[2]/div/ul/li[1]/a";
	
	public PaginaParametros clicoProduto() {
		paginaBase = new PaginaBase();	
		paginaBase.scroll();
		paginaBase.aguardarElemento(resultProduct);
		paginaBase.clicarXpath(resultProduct);
		return new PaginaParametros();
	}

	public PaginaParametros escolhaCor() {
		paginaBase = new PaginaBase();
		paginaBase.aguardarElemento(buttonColor);
		paginaBase.verificarCampoExisteClickXpath(buttonColorCSS, buttonColor);
		return new PaginaParametros();
	}
	
	public PaginaParametros tamanho() {
		paginaBase = new PaginaBase();
		paginaBase.aguardarElemento(buttonTamanho);
		paginaBase.verificarCampoExisteClickXpath(buttonTamanhoCSS, buttonTamanho);
		return new PaginaParametros();
	}
	

	public String tituloProduct() {
		paginaBase = new PaginaBase();	
		paginaBase.aguardarElemento(tituloProduct);
		return paginaBase.pegarValoXpath(tituloProduct);
	}
	
	public PaginaParametros adicionarCarrinho() {
		paginaBase = new PaginaBase();	
		paginaBase.scroll();
		paginaBase.aguardarElemento(buttonComprar);
		paginaBase.clicarXpath(buttonComprar);
		return new PaginaParametros();
	}

}
