package br.com.web.shoestock.page;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HomePagina extends PaginaBase{
	
	protected PaginaBase paginaBase;
	
	private String agenciaCampoPesquisaXpath = "//*[@id=\"search-input\"]";
	private String buttonPesquisarXpath = "//*[@id=\"header-content\"]/header/div/div/section[2]/section/form/div/button";
	private String resultXpath = "//*[@id=\"content\"]/section/section[2]/h1";
		
	public PaginaParametros campoBusca(String pesquisa) {
		paginaBase = new PaginaBase();
		paginaBase.aguardarElemento(agenciaCampoPesquisaXpath);
		paginaBase.digitarXpath(agenciaCampoPesquisaXpath, pesquisa);
		return new PaginaParametros();
	}

	public PaginaParametros clicoEmPesquisar() {
		paginaBase = new PaginaBase();
		paginaBase.aguardarElemento(buttonPesquisarXpath);
		paginaBase.clicarXpath(buttonPesquisarXpath);
		return new PaginaParametros();
	}
	
	public String validarResultado() {
		paginaBase = new PaginaBase();
		paginaBase.aguardarElemento(resultXpath);
		return paginaBase.pegarValoXpath(resultXpath);
	}
	
}
