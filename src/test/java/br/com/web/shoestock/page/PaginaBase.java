package br.com.web.shoestock.page;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PaginaBase {

	private static WebDriver driver;
	
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	
	public static WebDriver getDriver(){
		return driver;
	}
	
	public void AbrirNavegador() {
		DesiredCapabilities capability = new DesiredCapabilities();
		File file = new File(".\\src\\test\\resources\\utils\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		capability.setAcceptInsecureCerts(true);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
	}
	
	public void navegarPara(String url){
		this.driver.navigate().to(url);
	}
	
	public void tituloPagina(){
		this.driver.getTitle();
	}
	
	public void selecionarAba(int aba) {
		 ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		 this.driver.switchTo().window(tabs2.get(aba));
	}
	
	public void limparCampoXpath(String elemento) {
		this.driver.findElement(By.xpath(elemento)).clear();
	}
	
	public void digitarXpath(String elemento, String digitar) {
		this.driver.findElement(By.xpath(elemento)).sendKeys(digitar);
	}
	
	public void digitarID(String elemento, String digitar) {
		this.driver.findElement(By.id(elemento)).sendKeys(digitar);
	}
	
	public void selecionarOpcaoXpath(String elemento, String selecionar) {
		WebElement mySelectElement = driver.findElement(By.xpath(elemento));
		Select dropdown = new Select(mySelectElement);
		dropdown.selectByVisibleText(selecionar);
	}
	
	public void clicarXpath(String elemento) {
		this.driver.findElement(By.xpath(elemento)).click();
	}
	
	public void clicarCSSSelector(String elemento) {
		this.driver.findElement(By.cssSelector(elemento)).click();
	}
	
	public void verificarDigitacaoID(String elemento, String digitar) {
		for(int i=0; i < 2; i++ ) {
			if(this.driver.findElement(By.id(elemento)).getAttribute("value").equals("")) {
				try {
					Thread.sleep(500);
					this.driver.findElement(By.id(elemento)).clear();
					digitarID(elemento, digitar);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				i = 0;
			}else {
				i = 3;
			}
		}
	}
	
	public void verificarDigitacaoXpath(String elemento, String digitar) {
		for(int i=0; i < 2; i++ ) {
			if(this.driver.findElement(By.xpath(elemento)).getAttribute("value").equals("")) {
				try {
					Thread.sleep(500);
					this.driver.findElement(By.xpath(elemento)).clear();
					digitarXpath(elemento, digitar);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				i = 0;
			}else {
				i = 3;
			}
		}
	}
	
	public boolean verificarCampoExisteDigitarID(String campoVerificarCSS, String campoVerificarID, String valorDigitar) {
		this.driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		List<WebElement> dynamicElement = this.driver.findElements(By.cssSelector(campoVerificarCSS));
		if(dynamicElement.size() != 0) {
			verificarDigitacaoID(campoVerificarID, valorDigitar);
			return true;
		}else {
			this.driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
			return false;
		}
	}
	
	public boolean verificarCampoExisteDigitarXpath(String campoVerificarCSS, String campoVerificarXpath, String valorDigitar) {
		this.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		List<WebElement> dynamicElement = this.driver.findElements(By.cssSelector(campoVerificarCSS));
		if(dynamicElement.size() != 0) {
			verificarDigitacaoXpath(campoVerificarXpath, valorDigitar);
			return true;
		}else {
			this.driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
			return false;
		}
	}
	
	public boolean verificarCampoExisteClickXpath(String campoVerificarCSS, String campoVerificarXpath) {
		this.driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		List<WebElement> dynamicElement = this.driver.findElements(By.cssSelector(campoVerificarCSS));
		if(dynamicElement.size() != 0) {
			clicarXpath(campoVerificarXpath);
			return true;
		}else {
			this.driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			return false;
		}
	}
	
	public boolean verificarCampoExisteClickCSSSelector(String campoVerificarCSS) {
		this.driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		List<WebElement> dynamicElement = this.driver.findElements(By.cssSelector(campoVerificarCSS));
		if(dynamicElement.size() != 0) {
			clicarCSSSelector(campoVerificarCSS);
			return true;
		}else {
			this.driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			return false;
		}
	}
	
	public boolean verificarCampoExiste(String campoVerificarCSS) {
		this.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		List<WebElement> dynamicElement = this.driver.findElements(By.cssSelector(campoVerificarCSS));
		if(dynamicElement.size() != 0) {
			return true;
		}else {
			this.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			return false;
		}
	}
	
	public void fecharNavegador(){
		getDriver().close();
	}

	public String pegarValoXpath(String elemento) {
		return this.driver.findElement(By.xpath(elemento)).getText();
	}
	
	public void aguardarElemento(String elementoXpath) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(this.driver.findElement(By.xpath(elementoXpath))));
	}
	
	public void scroll() {
		JavascriptExecutor js = ((JavascriptExecutor) getDriver());
		js.executeScript("window.scrollBy(0,300)");
	}
}
