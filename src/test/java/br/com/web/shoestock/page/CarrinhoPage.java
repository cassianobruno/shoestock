package br.com.web.shoestock.page;

public class CarrinhoPage {
	
	protected PaginaBase paginaBase;
	
	private String tituloProduct = "/html/body/div[1]/div[2]/div[2]/div[1]/div[2]/div/div[1]/div/div/h3";

	public String ValidarProdutoIncluido() {
		paginaBase = new PaginaBase();	
		paginaBase.aguardarElemento(tituloProduct);
		return paginaBase.pegarValoXpath(tituloProduct);
	}
}
