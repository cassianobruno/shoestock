package br.com.web.shoestock.page;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class PaginaParametros extends PaginaBase{
	
	public String obterValorParametro(String id){
		return getDriver().findElement(By.id(id)).getAttribute("value");
	}
	
	public void aguardarProcessamento() {

		System.out.println("VAI AGUARDAR O PROCESSAMENTO");
		try {
			Thread.sleep(100);

			while (true) {
				boolean pageStatusIsComplete = (boolean) ((JavascriptExecutor) getDriver())
						.executeScript("return document.readyState").equals("complete");
				if (pageStatusIsComplete) {
					break;
				}
				Thread.sleep(100);
			}

			while (true) {
				boolean ajaxIsComplete = (boolean) ((JavascriptExecutor) getDriver())
						.executeScript("return jQuery.active == 0;");
				if (ajaxIsComplete) {
					break;
				}
				Thread.sleep(100);
			}

		} catch (RuntimeException | InterruptedException e) {
			
		}
		System.out.println("FIM DO PROCESSAMENTO");
	}
}
