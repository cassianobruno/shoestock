package br.com.web.shoestock.steps;

import br.com.web.shoestock.execution.RealizarBusca;
import br.com.web.shoestock.page.PaginaBase;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class RealizarBuscaSteps {

	RealizarBusca realizarBusca = new RealizarBusca();
	PaginaBase paginaBase;
	Scenario scenario;
	
	@Before
	public void before(Scenario scenario) {
	    this.scenario = scenario;
	    realizarBusca.setNomeDoCenario(scenario.getName());
	    realizarBusca.setNomeDaFeature(scenario.getId().split(";")[0].replace("-"," "));
	}
	
	@After("@RealizarBusca")
	public void leave_window_open(Scenario scenario) {
		if (scenario.isFailed()) {
	        paginaBase.getDriver().close();	 
	    }else {
	    	paginaBase.getDriver().close();
	    }
	}
	
	@Dado("^que estou na home page do Shoestock$")
	public void que_estou_na_home_page_do_Shoestock() throws Throwable {
	    realizarBusca.acessandoPaginaHome();
	}

	@Quando("^eu digito a \"([^\"]*)\"$")
	public void eu_digito_a(String pesquisa) throws Throwable {
		realizarBusca.digitarBusca(pesquisa);
	}

	@Quando("^eu clico em pesquisar$")
	public void eu_clico_em_pesquisar() throws Throwable {
		realizarBusca.clicoEmPesquisar();
	}

	@Então("^eu Valido o resultado da pesquisa$")
	public void eu_Valido_o_resultado_da_pesquisa() throws Throwable {
		realizarBusca.validarResultado();
	}

}
