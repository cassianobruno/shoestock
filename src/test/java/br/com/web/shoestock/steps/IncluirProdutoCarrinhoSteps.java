package br.com.web.shoestock.steps;

import br.com.web.shoestock.page.PaginaBase;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Então;
import br.com.web.shoestock.execution.IncluirProdutoCarrinho;
import br.com.web.shoestock.execution.RealizarBusca;

public class IncluirProdutoCarrinhoSteps {

	IncluirProdutoCarrinho incluirProdutoCarrinho = new IncluirProdutoCarrinho();
	RealizarBusca realizarBusca = new RealizarBusca();
	PaginaBase paginaBase;
	Scenario scenario;
	
	@Before
	public void before(Scenario scenario) {
	    this.scenario = scenario;
	    realizarBusca.setNomeDoCenario(scenario.getName());
	    realizarBusca.setNomeDaFeature(scenario.getId().split(";")[0].replace("-"," "));
	}
	
	@After("@IncluirProdutoNoCarrinho")
	public void leave_window_open(Scenario scenario) {
		if (scenario.isFailed()) {
	        paginaBase.getDriver().close();	 
	    }else {
	    	paginaBase.getDriver().close();	 
	    }
	}
	
	@Então("^eu clico no produto do resultado$")
	public void eu_clico_no_produto_do_resultado() throws Throwable {
		incluirProdutoCarrinho.clicoProduto();
	}

	@Então("^eu incluo o produto no carrinho$")
	public void eu_incluo_o_produto_no_carrinho() throws Throwable {
		incluirProdutoCarrinho.adicionarCarrinho();
	}
}
