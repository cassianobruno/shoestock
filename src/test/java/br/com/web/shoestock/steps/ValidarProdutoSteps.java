package br.com.web.shoestock.steps;

import br.com.web.shoestock.execution.RealizarBusca;
import br.com.web.shoestock.execution.ValidarProduto;
import br.com.web.shoestock.page.PaginaBase;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Então;

public class ValidarProdutoSteps {

	ValidarProduto validarProduto = new ValidarProduto();
	RealizarBusca realizarBusca = new RealizarBusca();
	PaginaBase paginaBase;
	Scenario scenario;
	
	@Before
	public void before(Scenario scenario) {
	    this.scenario = scenario;
	    realizarBusca.setNomeDoCenario(scenario.getName());
	    realizarBusca.setNomeDaFeature(scenario.getId().split(";")[0].replace("-"," "));
	}
	
	@After("@validar")
	public void leave_window_open(Scenario scenario) {
		if (scenario.isFailed()) {
	        paginaBase.getDriver().close();	 
	    }else {
	    	paginaBase.getDriver().close();	 
	    }
	}
	
	@Então("^valido os dados do produto$")
	public void valido_os_dados_do_produto() throws Throwable {
		validarProduto.ValidarProdutoIncluido();
	}
}
