package br.com.web.shoestock.execution;

import br.com.web.shoestock.feature.RealizarBuscaFeature;
import br.com.web.shoestock.page.*;

public class RealizarBusca {

	String nomeCenario;
	String nomeFeature;
	
	protected PaginaBase paginaBase;
	protected PaginaParametros paginaParametros;
	protected RealizarBuscaFeature realizarBuscaFeature;
	
	public void setNomeDoCenario(String nomeDoCenario) {
		this.nomeCenario = nomeDoCenario;
	}
	
	public void setNomeDaFeature(String nomeDaFeature) {
		this.nomeFeature = nomeDaFeature;
	}
	
	public void acessandoPaginaHome() {
		this.paginaBase = new PaginaBase();
		this.paginaBase.AbrirNavegador();
		this.paginaBase.navegarPara("https://www.shoestock.com.br/");
		
		try {
			Thread.sleep(5000);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void digitarBusca(String pesquisa) {
		this.realizarBuscaFeature = new RealizarBuscaFeature();
		this.paginaParametros = new PaginaParametros();
		this.paginaParametros.aguardarProcessamento();	
		this.realizarBuscaFeature.digitarBusca(pesquisa);
	}

	public void clicoEmPesquisar() {
		this.realizarBuscaFeature = new RealizarBuscaFeature();
		this.paginaParametros = new PaginaParametros();
		this.paginaParametros.aguardarProcessamento();	
		this.realizarBuscaFeature.clicoEmPesquisar();
		this.paginaParametros.aguardarProcessamento();	
	}
	

	public void validarResultado() {
		this.realizarBuscaFeature = new RealizarBuscaFeature();
		this.realizarBuscaFeature.validarResultado();
	}
	
	public void fechaNavegador() {
		this.paginaBase.fecharNavegador();
	}

}
