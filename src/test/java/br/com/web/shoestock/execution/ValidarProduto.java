package br.com.web.shoestock.execution;

import br.com.web.shoestock.page.PaginaBase;
import br.com.web.shoestock.page.PaginaParametros;
import br.com.web.shoestock.feature.ValidarProdutoFeature;

public class ValidarProduto {

	String nomeCenario;
	String nomeFeature;
	
	protected PaginaBase paginaBase;
	protected PaginaParametros paginaParametros;
	protected ValidarProdutoFeature validarProdutoFeature;
	
	public void setNomeDoCenario(String nomeDoCenario) {
		this.nomeCenario = nomeDoCenario;
	}
	
	public void setNomeDaFeature(String nomeDaFeature) {
		this.nomeFeature = nomeDaFeature;
	}

	public void ValidarProdutoIncluido() {
		this.validarProdutoFeature = new ValidarProdutoFeature();
		this.paginaParametros = new PaginaParametros();
		this.paginaParametros.aguardarProcessamento();
		this.validarProdutoFeature.ValidarProdutoIncluido();
		this.paginaParametros.aguardarProcessamento();
	}
}
