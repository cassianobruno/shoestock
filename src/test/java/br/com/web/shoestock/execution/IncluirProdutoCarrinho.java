package br.com.web.shoestock.execution;

import br.com.web.shoestock.page.PaginaBase;
import br.com.web.shoestock.page.PaginaParametros;
import br.com.web.shoestock.feature.IncluirProdutoCarrinhoFeature;

public class IncluirProdutoCarrinho {

	String nomeCenario;
	String nomeFeature;
	
	protected PaginaBase paginaBase;
	protected PaginaParametros paginaParametros;
	protected IncluirProdutoCarrinhoFeature incluirProdutoCarrinhoFeature;
	
	public void setNomeDoCenario(String nomeDoCenario) {
		this.nomeCenario = nomeDoCenario;
	}
	
	public void setNomeDaFeature(String nomeDaFeature) {
		this.nomeFeature = nomeDaFeature;
	}

	public void clicoProduto() {
		this.incluirProdutoCarrinhoFeature = new IncluirProdutoCarrinhoFeature();
		this.paginaParametros = new PaginaParametros();
		this.paginaParametros.aguardarProcessamento();
		this.incluirProdutoCarrinhoFeature.clicoProduto();
		this.paginaParametros.aguardarProcessamento();
	}

	public void adicionarCarrinho() {
		this.incluirProdutoCarrinhoFeature = new IncluirProdutoCarrinhoFeature();
		this.paginaParametros = new PaginaParametros();
		this.paginaParametros.aguardarProcessamento();
		this.incluirProdutoCarrinhoFeature.tituloProduct();
		this.paginaParametros.aguardarProcessamento();
		this.incluirProdutoCarrinhoFeature.adicionarCarrinho();
		this.paginaParametros.aguardarProcessamento();
	}
}
