<!-- PROJECT SHOESTOCK-->
<br />
<p align="center">
  <a href="https://bitbucket.org/cassianobruno/shoestock">LINK</a>

  <h3 align="center">Best-README-Template</h3>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->

### Installation

1. Get a free
2. Clone the repo
   ```sh
   git clone https://bitbucket.org/cassianobruno/shoestock.git
   ```

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Bruno Cassiano - bruno15l@hotmail.com

Project Link: [https://bitbucket.org/cassianobruno/shoestock]